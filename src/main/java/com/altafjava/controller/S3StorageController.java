package com.altafjava.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.altafjava.service.S3StorageService;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;

@RestController
public class S3StorageController {

	@Autowired
	private S3StorageService s3StorageService;

	@GetMapping("/buckets")
	public List<Bucket> listBuckets() {
		return s3StorageService.listBuckets();
	}

	@GetMapping("/list-objects")
	public ObjectListing listObjects() {
		return s3StorageService.listObjects();
	}

	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String upload(@RequestPart MultipartFile multipartFile) {
		return s3StorageService.upload(multipartFile);
	}

	@GetMapping("/download")
	public ResponseEntity<ByteArrayResource> download(@RequestParam String fileName) throws IOException {
		return s3StorageService.download(fileName);
	}

	@DeleteMapping("/delete")
	public String delete(@RequestParam String fileName) {
		return s3StorageService.delete(fileName);
	}
}
