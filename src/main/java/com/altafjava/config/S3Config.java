package com.altafjava.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
public class S3Config {

	@Value("${aws.s3.access.key.id}")
	private String accessKey;
	@Value("${aws.s3.secret.access.key}")
	private String secretKey;

	@Bean
	public AmazonS3Client getAmazonS3Client() {
		AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3Client amazonS3Client = new AmazonS3Client(awsCredentials);
		return amazonS3Client;
	}

}
