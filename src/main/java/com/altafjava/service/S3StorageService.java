package com.altafjava.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;

@Service
public class S3StorageService {
	@Autowired
	private AmazonS3Client amazonS3Client;
	@Value("${aws.s3.bucket-name}")
	private String bucketName;

	public List<Bucket> listBuckets() {
		List<Bucket> listBuckets = amazonS3Client.listBuckets();
		return listBuckets;
	}

	public ObjectListing listObjects() {
		ObjectListing objectListing = amazonS3Client.listObjects(bucketName);
		return objectListing;
	}

	public String upload(@RequestPart MultipartFile multipartFile) {
		String baseDir = "src/main/resources/";
		File file = new File(baseDir + multipartFile.getName());
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(multipartFile.getBytes());
			fos.close();
			amazonS3Client.putObject(bucketName, multipartFile.getOriginalFilename(), file);
		} catch (IOException e) {
			return multipartFile.getOriginalFilename() + " upload failed";
		}
		return multipartFile.getOriginalFilename() + " uploaded successfully!";
	}

	public ResponseEntity<ByteArrayResource> download(@RequestParam String fileName) throws IOException {
		S3Object s3Object = amazonS3Client.getObject(bucketName, fileName);
		if (s3Object != null) {
			S3ObjectInputStream s3ObjectInputStream = s3Object.getObjectContent();
			byte[] content = IOUtils.toByteArray(s3ObjectInputStream);
			s3Object.close();
			ByteArrayResource byteArrayResource = new ByteArrayResource(content);
			return ResponseEntity.ok().contentLength(content.length).header("Content-type", "application/octet-stream")
					.header("Content-disposition", "attachment; filename=\"" + fileName + "\"").body(byteArrayResource);
		}
		return null;
	}

	public String delete(@RequestParam String fileName) {
		amazonS3Client.deleteObject(bucketName, fileName);
		return fileName + " deleted successfully";
	}

}
